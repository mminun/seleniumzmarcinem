import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class RybaTest extends FunctionalTest {

    private static SignupPage signupPage;

    @BeforeClass
    public static void setUp() {
        FunctionalTest.setUp();
        signupPage = new SignupPage(driver);
    }

    @Test
    public void rybaTest(){
        driver.get(properties.getProperty("questURL"));

        assertTrue(signupPage.isInitialized());
    }

    @Test
    public void isLogoPresent(){
        driver.get(properties.getProperty("questURL"));

        assertTrue(signupPage.isInitialized());
    }
}
