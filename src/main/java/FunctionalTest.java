import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class FunctionalTest {
    static WebDriver driver;
    static Properties properties;

    @BeforeClass
    public static void setUp() {
        properties = loadProperties();
        String browserName = properties.getProperty("browser");
        String browserDriver = properties.getProperty(String.format("%sDriver", browserName));
        writeToSystemProperties(browserName, browserDriver);
        assignDriver(browserName);
        setDriverTimeout();
    }

    private static void assignDriver(String browserName) {
        driver = RemoteWebDriverCreator.valueOf(browserName.toUpperCase()).createDriver();
    }

    private static void writeToSystemProperties(String browserName, String browserDriver) {
        System.setProperty(String.format("webdriver.%s.driver", browserName), browserDriver);
    }

    private static void setDriverTimeout() {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    }

    private static Properties loadProperties() {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(Constants.PROP_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }

    @After
    public void cleanUp() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

    private enum RemoteWebDriverCreator {
        CHROME {
            @Override
            RemoteWebDriver createDriver() {
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--test-type");
                chromeOptions.addArguments("--disable-extensions");
                return new ChromeDriver(chromeOptions);
            }
        },
        GECKO {
            @Override
            RemoteWebDriver createDriver() {
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                return new FirefoxDriver(firefoxOptions);
            }
        },
        EDGE {
            @Override
            RemoteWebDriver createDriver() {
                EdgeOptions edgeOptions = new EdgeOptions();
                return new EdgeDriver(edgeOptions);
            }
        };

        abstract RemoteWebDriver createDriver();

    }
}
