import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignupPage extends PageObject {

    @FindBy(id = "LogoContent")
    private WebElement logo;

    @FindBy(css = "a[href*='FISH']")
    private WebElement ryba;


    public SignupPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return logo.isDisplayed();
    }
    public void clickOnFish() {
        this.ryba.click();
    }

}
